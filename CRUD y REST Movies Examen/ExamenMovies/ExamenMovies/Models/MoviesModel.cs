﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenMovies.Models
{
    public class MoviesModel
    {
        [Key]
        public int movie_id { get; set; }

        [Required(ErrorMessage = "Enter the Movie name")]
        [Display(Name = "Movie Name")]
        public string movie_name { get; set; }

        [Required(ErrorMessage = "Enter the genere of the movie")]
        [Display(Name = "Genere")]
        public string movie_genere { get; set; }

        [Required(ErrorMessage = "Enter the release year of the movie")]
        [Display(Name = "Year")]
        public int movie_year { get; set; }

        [Required(ErrorMessage = "Enter the calification of the movie")]
        [Display(Name = "Calification")]
        public string movie_calification { get; set; }
    }
}

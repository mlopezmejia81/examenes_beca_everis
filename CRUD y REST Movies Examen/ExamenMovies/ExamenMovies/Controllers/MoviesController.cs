﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamenMovies.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ExamenMovies.Controllers
{
    public class MoviesController : Controller
    {
        private readonly ApplicationDbContext _db;

        public MoviesController(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IActionResult> Index(string searchString)
        {


            // var displaydata = _db.ClientDb.ToList();// es _db.nombre de la tabla

            var getMoviesdetail = from m in _db.Movies
                                  select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                getMoviesdetail = getMoviesdetail.Where(s => s.movie_name.Contains(searchString));
            }


            return View(await getMoviesdetail.ToListAsync());
        }
        
        public async Task<ActionResult> Edit(int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
;
            }

            var getMovie = await _db.Movies.FindAsync(id);
            return View(getMovie);
        }

        [HttpPost]
        public async Task<ActionResult> Edit (MoviesModel oldMovie)
        {
            if(ModelState.IsValid)
            {
                _db.Update(oldMovie);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldMovie);

        }

        public async Task<ActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getClient = await _db.Movies.FindAsync(id);
            return View(getClient);
        }


        public async Task<ActionResult> Delete(int? id)
        {

            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getClient = await _db.Movies.FindAsync(id);
            return View(getClient);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {

            var getClient = await _db.Movies.FindAsync(id);
            _db.Movies.Remove(getClient);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");


        }

        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> Create(MoviesModel nuevo) //se le pasa algo del tipo del modelo!!!!!!!!!!!
        {
            if (ModelState.IsValid)
            {
                _db.Add(nuevo);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }

            return View(nuevo);
        }
    }
}

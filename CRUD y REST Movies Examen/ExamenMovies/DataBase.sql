 CREATE TABLE ClientDb(
	client_id int not null primary key identity(1,1),
	client_name nvarchar(150),
	client_credit nvarchar(150),
	client_age int,
	client_email nvarchar(150),
);


insert into ClientDb (client_name,client_credit,client_age,client_email)
values('angel','Si tiene', 24, 'angel@gmail.com')

select * from ClientDb

use Examen


 CREATE TABLE Movies(
	movie_id int not null primary key identity(1,1),
	movie_name nvarchar(150),
	movie_genere nvarchar(150),
	movie_year int,
	movie_calification nvarchar(150),
);

insert into Movies (movie_name,movie_genere,movie_year,movie_calification)
values('Avengers','Action', 2019, 'Muy Buena')

select * from Movies
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RESTExamenMovies.Models
{
    [MetadataType(typeof(Movy.MetaData))]
    public partial class Movy
    {


        sealed class MetaData
        {
            [Key]
            public int movie_id;

            [Required(ErrorMessage = "Enter the Movie name")]
            public string movie_name;

            [Required(ErrorMessage = "Enter the genere of the movie")]
            public string movie_genere;

            [Required(ErrorMessage = "Enter the release year of the movie")]
            public Nullable<int> movie_year;

            [Required(ErrorMessage = "Enter the calification of the movie")]
            public string movie_calification;
        }
    }
}